package immobilierexperiencems.model.request;

import immobilierexperiencems.constant.SalaryRange;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoanRequest {
    private String id;
    @Size(min=5,max=30,message = "Enter name with size between 5 & 30")
    private String name;
    private int age;
    @Pattern(regexp = "^\\d{10}$",message = "The NationalId must be 10 digits")
    private String nationalId;
    @Min(value = 600,message = " Loan amount must be in the Limit of 600 and above")
    @Max(value = 2000,message = "Loan amount must be in the Limit of 2000 and below")
    private float loanAmount;
    private int tenure;
    private SalaryRange salaryRangeEnum;
}
