package immobilierexperiencems.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanUpdate {
    @Min(value = 600,message = " Loan amount must be in the Limit of 600 and above")
    @Max(value = 2000,message = "Loan amount must be in the Limit of 2000 and below")
    private float loanAmount;
    private int tenure;
}
