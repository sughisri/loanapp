package immobilierexperiencems.model.response;

import immobilierexperiencems.constant.LoanApplicationStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanStatus {
    private String id;
    private LoanApplicationStatus statusEnum;
}
