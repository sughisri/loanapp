package immobilierexperiencems;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ImmobilierExperienceMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImmobilierExperienceMsApplication.class, args);
	}

}
