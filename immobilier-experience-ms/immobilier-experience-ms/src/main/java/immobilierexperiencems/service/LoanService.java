package immobilierexperiencems.service;

import immobilierexperiencems.constant.CountryEnum;
import immobilierexperiencems.exception.InvalidLoanAmountException;
import immobilierexperiencems.exception.InvalidTenureException;
import immobilierexperiencems.model.request.LoanRequest;
import immobilierexperiencems.model.request.LoanUpdate;
import immobilierexperiencems.model.response.LoanResponse;
import immobilierexperiencems.model.response.LoanStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class LoanService {
    @Autowired
    FeignService feignService;

    public ArrayList<String> addTenure(CountryEnum country, String uuid) {
        return feignService.addLoan(country, uuid);
    }

    public LoanResponse addLoan(CountryEnum country, String uuid, LoanUpdate loanUpdate) {
        if (loanUpdate.getLoanAmount() > 599 || loanUpdate.getLoanAmount() < 2001) {
            tenureCheck(loanUpdate.getTenure());
            return feignService.addingLoan(country, uuid, loanUpdate);
        } else {
            throw new InvalidLoanAmountException();
        }

    }

    public LoanStatus conformLoan(CountryEnum country, String uuid, LoanRequest loanRequest) {
        if (loanRequest.getLoanAmount() > 599 || loanRequest.getLoanAmount() < 2001) {
            tenureCheck(loanRequest.getTenure());
            return feignService.conformingLoan(country, uuid, loanRequest);
        } else {
            throw new InvalidLoanAmountException();
        }
    }


    public void tenureCheck(int tenure) {
        if (tenure == 12 || tenure == 24 || tenure == 36 || tenure == 48 || tenure == 60) {
        } else {
            throw new InvalidTenureException();
        }
    }
}