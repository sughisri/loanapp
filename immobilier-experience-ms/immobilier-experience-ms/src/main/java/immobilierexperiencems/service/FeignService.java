package immobilierexperiencems.service;

import immobilierexperiencems.constant.CountryEnum;
import immobilierexperiencems.model.request.LoanRequest;
import immobilierexperiencems.model.request.LoanUpdate;
import immobilierexperiencems.model.response.LoanResponse;
import immobilierexperiencems.model.response.LoanStatus;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.ArrayList;

@FeignClient(name = "loanApplication" ,url = "${config.rest.service.getLoanManagementUrl}")
public interface FeignService {
    @GetMapping("/getTenure")
    public ArrayList<String> addLoan(@RequestHeader("CountryCode")CountryEnum country,@RequestHeader("uuid")String uuid);
    @PostMapping("/loanDetails")
    LoanResponse addingLoan(@RequestHeader("CountryCode")CountryEnum country,@RequestHeader("uuid")String uuid ,@RequestBody LoanUpdate loanUpdate);
    @PostMapping("/loanConformation")
    public LoanStatus conformingLoan(@RequestHeader("CountryCode")CountryEnum country, @RequestHeader("uuid")String uuid, @RequestBody LoanRequest loanRequest);
}

