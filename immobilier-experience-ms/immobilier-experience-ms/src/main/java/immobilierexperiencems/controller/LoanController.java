package immobilierexperiencems.controller;

import immobilierexperiencems.constant.CountryEnum;
import immobilierexperiencems.exception.InvalidLoanAmountException;
import immobilierexperiencems.model.request.LoanRequest;
import immobilierexperiencems.model.request.LoanUpdate;
import immobilierexperiencems.model.response.LoanResponse;
import immobilierexperiencems.model.response.LoanStatus;
import immobilierexperiencems.service.LoanService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
@Slf4j
@RestController
@RequestMapping
public class LoanController {
    @Autowired
    LoanService loanService;
    @GetMapping("/tenure")
    public ArrayList<String>getTenure(@RequestHeader("CountryCode")CountryEnum country, @RequestHeader("uuid")String uuid){
        log.info("experienceLayer get tenure :  uuid - " +uuid +"  countryCode  -  "   +country);
        return  loanService.addTenure(country,uuid);
    }
    @PostMapping("/addLoan")
    public ResponseEntity<LoanResponse> addLoan(@RequestHeader("CountryCode")CountryEnum country ,@RequestHeader("uuid")String uuid,@Valid  @RequestBody LoanUpdate loanUpdate){
       try{
           log.info("experienceLayer addLoan :  uuid - " +uuid +"  countryCode  -  "   +country);
           return  new ResponseEntity(loanService.addLoan(country, uuid, loanUpdate), HttpStatus.OK);
       }catch(InvalidLoanAmountException i){
           return  new ResponseEntity(i.getMessage(),HttpStatus.BAD_REQUEST);
       }
    }
    @PostMapping("/conformLoan")
    public ResponseEntity<LoanStatus>loanConform(@RequestHeader("CountryCode")CountryEnum country, @RequestHeader("uuid")String uuid, @Valid @RequestBody LoanRequest loanRequest){
       try{
           log.info("experienceLayer conformLoan :  uuid - " +uuid +"  countryCode  -  "   +country);
           return new ResponseEntity<>(loanService.conformLoan(country,uuid,loanRequest),HttpStatus.OK);
       }catch (InvalidLoanAmountException i){
           return new ResponseEntity(i.getMessage(),HttpStatus.BAD_REQUEST);
       }
    }

}
