package immobilierexperiencems.exception;

public class InvalidTenureException extends RuntimeException{
     public InvalidTenureException(){
        super("Invalid Tenure is invalid");
    }
}
