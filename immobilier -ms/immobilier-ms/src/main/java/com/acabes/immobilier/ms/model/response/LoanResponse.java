package com.acabes.immobilier.ms.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanResponse {
    private float loanAmount;
    private int tenure;
    private float installment;
    private float interest;
    private float totalInterestRate;
    private float fee;
    private float total;
}
