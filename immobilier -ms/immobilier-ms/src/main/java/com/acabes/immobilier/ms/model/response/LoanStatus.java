package com.acabes.immobilier.ms.model.response;

import com.acabes.immobilier.ms.constant.LoanApplicationStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoanStatus {
    private String id;
    private LoanApplicationStatus statusEnum;
}
