package com.acabes.immobilier.ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImmobilierMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImmobilierMsApplication.class, args);
	}

}
