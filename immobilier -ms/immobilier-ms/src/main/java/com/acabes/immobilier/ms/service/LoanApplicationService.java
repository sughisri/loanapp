package com.acabes.immobilier.ms.service;

import com.acabes.immobilier.ms.constant.CountryEnum;
import com.acabes.immobilier.ms.constant.LoanApplicationStatus;
import com.acabes.immobilier.ms.model.entity.LoanEntity;
import com.acabes.immobilier.ms.model.request.LoanRequest;
import com.acabes.immobilier.ms.model.request.LoanUpdate;
import com.acabes.immobilier.ms.model.response.LoanResponse;
import com.acabes.immobilier.ms.model.response.LoanStatus;
import com.acabes.immobilier.ms.repository.LoanApplicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;

import static com.acabes.immobilier.ms.constant.LoanCalculationConstant.*;

@Service
public class LoanApplicationService {
    @Autowired
    LoanApplicationRepository loanApplicationRepository;
    @Autowired
    LoanCalculation loanCalculation;
    // get tenurelist
    public ArrayList<String>tenureList(CountryEnum country,String uuid) {
        ArrayList<String> tenure = new ArrayList<>();
        tenure.add("60 Months");
        tenure.add("48 Months");
        tenure.add("36 Months");
        tenure.add("24 Months");
        tenure.add("12 Months");
        return tenure;
    }
    //checking loan amount with tenure
    public LoanResponse getLoanDetails(CountryEnum country, String uuid, LoanUpdate loanUpdate){
       float loanInterestRate=0;
       float fee=0;
       switch (loanUpdate.getTenure()){
           case 60:
                loanInterestRate=SIXTY_INTEREST_RATE ;
                fee           = SIXTY_INTEREST_FEES;
                break;
           case 48:
              loanInterestRate =  FORTY_EIGHT_INTEREST_RATE;
               fee             = FORTY_EIGHT_INTEREST_FEES;
               break;
           case 36:
               loanInterestRate=THIRTY_SIX_INTEREST_RATE ;
               fee             = THIRTY_SIX_INTEREST_FEES;
               break;
           case 24:
               loanInterestRate=TWENTY_FOUR_INTEREST_RATE ;
               fee             = TWENTY_FOUR_INTEREST_FEES  ;
               break;
           case 12:
               loanInterestRate = TWELVE_INTEREST_RATE ;
               fee              = TWELVE_INTEREST_FEES;
               break;
       }
        LoanEntity loanEntity=new LoanEntity();
       float  installment=loanCalculation.calculation(loanUpdate.getLoanAmount(),loanUpdate.getTenure(),loanInterestRate);
       float totalAmount=installment * loanUpdate.getTenure();
       float totalInterestAmount = (int) Math.round(totalAmount-loanUpdate.getLoanAmount());
       LoanResponse loanResponse =new LoanResponse(loanUpdate.getLoanAmount(),loanUpdate.getTenure(),installment,loanInterestRate,totalInterestAmount,fee,(totalAmount+fee));
        loanEntity.setUuid(uuid);
        loanEntity.setCountriesEnums(CountryEnum.BH);
       loanApplicationRepository.save(loanEntity);
       return loanResponse;
    }
    public LoanStatus loanConform(CountryEnum country,String uuid ,LoanRequest loanRequest){
        float loanInterestRate=0;
        float fee=0;
        switch (loanRequest.getTenure()){
            case 60:
                loanInterestRate=SIXTY_INTEREST_RATE ;
                fee           = SIXTY_INTEREST_FEES;
                break;
            case 48:
                loanInterestRate =  FORTY_EIGHT_INTEREST_RATE;
                fee             = FORTY_EIGHT_INTEREST_FEES;
                break;
            case 36:
                loanInterestRate=THIRTY_SIX_INTEREST_RATE ;
                fee             = THIRTY_SIX_INTEREST_FEES;
                break;
            case 24:
                loanInterestRate=TWENTY_FOUR_INTEREST_RATE ;
                fee             = TWENTY_FOUR_INTEREST_FEES  ;
                break;
            case 12:
                loanInterestRate = TWELVE_INTEREST_RATE ;
                fee              = TWELVE_INTEREST_FEES;
                break;
        }
        float  installment=loanCalculation.calculation(loanRequest.getLoanAmount(),loanRequest.getTenure(),loanInterestRate);
        float totalAmount=installment * loanRequest.getTenure();
        float totalInterestAmount = (int) Math.round(totalAmount-loanRequest.getLoanAmount());
         LoanEntity loanEntity =new LoanEntity();
         loanEntity.setName(loanRequest.getName());
         loanEntity.setAge(loanRequest.getAge());
         loanEntity.setNationalId(loanRequest.getNationalId());
         loanEntity.setLoanAmount(loanRequest.getLoanAmount());
         loanEntity.setTenure(loanRequest.getTenure());
         loanEntity.setInstallment(installment);
         loanEntity.setTotalInterestAmt(totalInterestAmount);
         loanEntity.setSalaryRangeEnum(loanRequest.getSalaryRangeEnum());
         loanEntity.setFee(fee);
         loanEntity.setTotal(totalAmount);
         loanEntity.setCreateBy(loanRequest.getId());
         loanEntity.setInterest(loanInterestRate);
         loanEntity.setStatusEnum(LoanApplicationStatus.CONFIRMED);
         loanEntity.setCreatedDate(LocalDateTime.now());
          loanEntity.setUuid(uuid);
          loanEntity.setCountriesEnums(CountryEnum.BH);
          LoanEntity  response = loanApplicationRepository.save(loanEntity);
       return  new LoanStatus(response.getId(),response.getStatusEnum());
    }
}
