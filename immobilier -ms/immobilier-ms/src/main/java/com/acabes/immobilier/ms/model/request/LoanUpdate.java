package com.acabes.immobilier.ms.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanUpdate {
    private float loanAmount;
    private int tenure;
}
