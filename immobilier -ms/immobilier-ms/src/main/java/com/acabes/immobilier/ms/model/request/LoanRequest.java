package com.acabes.immobilier.ms.model.request;

import com.acabes.immobilier.ms.constant.SalaryRange;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoanRequest {
    private String id;
    private String name;
    private int age;
    private String nationalId;
    private float loanAmount;
    private int tenure;
    private SalaryRange salaryRangeEnum;

}
