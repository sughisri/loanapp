package com.acabes.immobilier.ms.model.entity;

import com.acabes.immobilier.ms.constant.CountryEnum;
import com.acabes.immobilier.ms.constant.LoanApplicationStatus;
import com.acabes.immobilier.ms.constant.SalaryRange;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "loanApplication")
public class LoanEntity {
    private String id;
    private String name;
    private int age;
    private String nationalId;
    private float loanAmount;
    private int tenure;
    private float installment;
    private float interest;
    private float totalInterestAmt;
    private float fee;
    private float total;
    private SalaryRange salaryRangeEnum;
    private LoanApplicationStatus statusEnum;
    private LocalDateTime createdDate;
    private String createBy;
    private String uuid;
    private CountryEnum countriesEnums;
}


