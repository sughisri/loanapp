package com.acabes.immobilier.ms.service;

import org.springframework.stereotype.Service;

@Service
public class LoanCalculation {
         int calculation(float principal,float month,float interestRate){
             float time =month/12;
             interestRate=interestRate/(12 * 100);
             time=time*12;
             Double  result=(principal * interestRate * Math.pow(1+interestRate,time))/(Math.pow(1+interestRate,time)-1);
             int installment= (int) Math.round(result);
             return installment;



         }
}
