package com.acabes.immobilier.ms.repository;

import com.acabes.immobilier.ms.model.entity.LoanEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface LoanApplicationRepository extends MongoRepository<LoanEntity,String> {
}
