package com.acabes.immobilier.ms.controller;

import com.acabes.immobilier.ms.constant.CountryEnum;
import com.acabes.immobilier.ms.model.request.LoanRequest;
import com.acabes.immobilier.ms.model.request.LoanUpdate;
import com.acabes.immobilier.ms.model.response.LoanResponse;
import com.acabes.immobilier.ms.model.response.LoanStatus;
import com.acabes.immobilier.ms.service.LoanApplicationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
@Slf4j
@RestController
@RequestMapping("/loanApplication")
public class LoanApplicationController {
    @Autowired
    LoanApplicationService loanApplicationService;

    @GetMapping("/getTenure")
    public ArrayList<String> getTenure(@RequestHeader("countryCode") CountryEnum country, @RequestHeader("uuid") String uuid) {
        log.info("managementLayer getTenure :  uuid - " +uuid +"  countryCode  -  "   +country);
        return loanApplicationService.tenureList(country, uuid);
    }
     @PostMapping("/loanDetails")
    public ResponseEntity<LoanResponse>postDetails(@RequestHeader("countryCode")CountryEnum country, @RequestHeader("uuid")String uuid, @RequestBody LoanUpdate loanUpdate){
         log.info("managementLayer addLoan :  uuid - " +uuid +"  countryCode  -  "   +country);
        return new ResponseEntity(loanApplicationService.getLoanDetails(country, uuid, loanUpdate), HttpStatus.OK);
     }
     @PostMapping("/loanConformation")
    public ResponseEntity<LoanStatus>loanConform(@RequestHeader("countryCode")CountryEnum country,@RequestHeader("uuid")String uuid,@RequestBody LoanRequest loanRequest){
         log.info("managementLayer LoanConformation :  uuid - " +uuid +"  countryCode  -  "   +country);
        return  new ResponseEntity(loanApplicationService.loanConform(country, uuid, loanRequest),HttpStatus.OK);
     }
}